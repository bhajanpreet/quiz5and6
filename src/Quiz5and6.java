
public class Quiz5and6 {
	public static void main(String[] args) {
		// Requirement #1
		System.out.println(studying("Shivam"));

		// Requirement #2
		System.out.println(studying(null));

		// Requirement #3
		System.out.println(studying("ROMIL"));

		// Requirement #4
		System.out.println(studying("Romil,Joao"));
		System.out.println(studying("Naveen,Carolini"));
		System.out.println(studying("NAVEEN,CAROLINI"));
	}

	private static String studying(String name) {
		String returnString = "";
		if (name == null) {
			returnString = "Nobody is studying";
		} else if (containsComma(name) > 0) {
			int indexComma = containsComma(name);
			String substr1 = name.substring(0, indexComma);
			String substr2 = name.substring(indexComma + 1);
			returnString = substr1 + " and " + substr2 + " are studying";
		} else if (name.equals(name.toUpperCase())) {
			returnString = (name + " is studying").toUpperCase();
		} else {
			returnString = name + " is studying";
		}
		return returnString;
	}
	
	public static int containsComma(String str) {
		int res = -1;
		
		for (int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == ',') {
				res = i;
				break;
			}
		}
		return res;
	}
}
